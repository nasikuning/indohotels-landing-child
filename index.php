<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="asset/img/favicon.ico">
	<title>Duta Garden Hotel - an Oasis in Town</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	    crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	    crossorigin="anonymous">
	<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="all">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

	<link href="https://file.myfontastic.com/YeqAUGvKegYFPh67bYrYw3/icons.css" rel="stylesheet">
	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body style="background-image: url('<?php echo the_field('background'); ?>')">
	<div class="landing">
		<div class="container-fluid">
			<div class="wrap">
				<div class="row">
					<?php get_template_part('loop'); ?>
				</div>
				<!-- end .row -->
			</div>
			<div class="col-md-12">
				<div class="form booking">
					<?php echo the_field('intro'); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- end .landing -->
</body>

</html>
